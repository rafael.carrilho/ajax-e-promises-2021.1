const url = "https://treinamento-ajax-api.herokuapp.com/messages"

// CONCEITUALIZANDO (EX 1)
// let imagem = downloadImagem(urlDaImagem)
// console.log(imagem)
// $undefined ou promise fulfilled (pendente)

// APRESENTANDO (EX 2)
// downloadImagem(urlDaImagem)
// .then(funçãoCallbackQuePrintaAImagem)
// .catch(funçãoCallbackQueResolveOErro)


// CONCLUINDO (EX 3)
// downloadImagem(urlDaImagem)
// .then(imagem => {
//     console.log(imagem)
// })
// .catch(error => {
//     console.warn(error)
// })


// GET PARA A API
// fetch(url+"/8")
// .then(resposta => resposta.json())
// .then(message => console.log(message))
// .catch(error => console.warn(error))


// POST PARA A API
// let botao = document.querySelector("#submitForm")
// botao.addEventListener("click", () => {

//     let fetchBody = {
//         "message":{
//             "name": "Rafael Carrilho",
//             "message": "Eu sou o instrutor de Ruby on Rails"
//         }
//     }
//     let fetchConfig = {
//         method: "POST",
//         headers: {
//             "Content-Type": "application/json"
//         },
//         body: JSON.stringify(fetchBody) 
//     }
//     fetch(url, fetchConfig)
//     .then(resposta => resposta.json())
//     .then(message => console.log(message))
//     .catch(error => console.warn(error))

    
// })


// DELETE PARA A API
// let botao = document.querySelector("#submitForm")
// botao.addEventListener("click", () => {
//     let fetchConfig = {
//         method: "DELETE" 
//     }
//     fetch(url+"/25", fetchConfig)
//     .then(() => console.log("apago memo"))
//     .catch(error => console.warn(error))

    
// })


// PUT PARA A API
// let botao = document.querySelector("#submitForm")
// botao.addEventListener("click", () => {

//     let fetchBody = {
//         "message":{
//             "message": "Eu tambem sou o instrutor de Wordpress"
//         }
//     }
//     let fetchConfig = {
//         method: "PUT",
//         headers: {
//             "Content-Type": "application/json"
//         },
//         body: JSON.stringify(fetchBody) 
//     }
//     fetch(url+"/8", fetchConfig)
//     .then(resposta => resposta.json())
//     .then(message => console.log(message))
//     .catch(error => console.warn(error))    
// })


