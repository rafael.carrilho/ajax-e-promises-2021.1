// DECLARAÇÃO DE CONSTANTES
const $ = document
const list = $.querySelector("#myList")
const url = "https://treinamento-ajax-api.herokuapp.com/messages"

// TRANSFORMAÇÃO DE MENSAGEM EM CARD
const transformDiv = (name, message, id) => {
    let new_child = $.createElement("div")
    new_child.id = "div-group-" + id
    new_child.classList.add("div_group")
    new_child.innerHTML= `
        <button onclick="toggleFormView(${id})">Editar</button>
        <button onclick="destroyMessage(${id})">Deletar</button>
        <h1> ${name} </h1>
        <p> ${message} </p>
        <div class="form_edit" > 
            <input type="text" id="name-edit-${id}" value="${name}"/>
            <input type="text" id="message-edit-${id}" value="${message}"/>    
            <button onclick="onSaveEdit(${id})"> Salvar </button>                    
            <button onclick="toggleFormView(${id})"> Cancelar </button>                    
        </div>`
    list.appendChild(new_child)
}

// GET
fetch(url)
.then(resp => resp.json())
.then(messages => {
    messages.forEach(element => {
        transformDiv(element.name, element.message, element.id)
    });
})
.catch(error => console.log(error))
// POST
$.querySelector("#myForm").addEventListener("submit", (e) => {
    e.preventDefault()
    let name = $.querySelector("#name").value
    let message = $.querySelector("#message").value
    if (name != "" && message != ""){
        let fetchBody = {
            "message":{
                "name": name,
                "message": message
            }
        }
        let fetchConfig = {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(fetchBody)
        }
        fetch(url, fetchConfig)
        .then(resp => resp.json())
        .then(element => transformDiv(element.name, element.message, element.id))
        .catch(error => console.warn(error))
    }
})

// PUT/PATCH
const toggleFormView = (id) =>{
    let form_edit = $.querySelector(`#div-group-${id}`).lastChild
    form_edit.style.display = form_edit.style.display === "none" || form_edit.style.display === "" ? "flex" : "none" 
}

const onSaveEdit = (id) => {
    let form_edit = $.querySelector(`#div-group-${id}`).lastChild
    let name_edit = $.querySelector(`#name-edit-${id}`).value //esse e o input de nome do formulario de edicao
    let message_edit = $.querySelector(`#message-edit-${id}`).value //esse e o input de nome do formulario de edicao
    
    if (name_edit != "" && message_edit != ""){
        let fetchBody = {
            "message":{
                "name": name_edit,
                "message": message_edit
            }
        }
        let fetchConfig = {
            method: "PUT",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(fetchBody)
        }
        
        fetch(url+"/"+id, fetchConfig)
        .then(resp => resp.json())
        .then(element => {
            form_edit.parentNode.querySelector("p").innerHTML = element.message
            form_edit.parentNode.querySelector("h1").innerHTML = element.name    
        })
        .catch(error => console.warn(error))
    }
}

// DELETE
const destroyMessage = (id) => {
    let fetchConfig = {
        method:"DELETE"
    }
    fetch(url+"/"+id, fetchConfig)
    .then(() => {
        let trash = $.querySelector(`#div-group-${id}`)
        trash.remove()
    })
    .catch(error => console.warn(error))
}

const destroyAllMessages = (e) => {
    list.innerHTML=" "
}